<?php 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('home/user/check', 'API\Auth\LoginController@UserExits');
Route::post('home/user/initiate', 'API\Auth\RegisterController@userInitiateRegistration');
Route::post('home/user/login', 'API\Auth\LoginController@userLogin');
Route::post('home/user/register', 'API\Auth\RegisterController@userCompleteRegistration');
Route::post('home/user/addAddress','API\AddressController@add');
Route::post('home/user/delAddress','API\AddressController@delete');
Route::post('home/user/updateAddress','API\AddressController@update');
Route::post('home/user/getAddresses','API\AddressController@getAaddresses');
Route::get('home/user/CategoryList','API\CategoryController@getCategorylist');
Route::get('home/user/bodytypeList','API\CategoryController@getBodytypelist');
Route::get('home/user/timelist','API\TimeController@getTimelist');

/*
|--------------------------------------------------------------------------
| Chat API Routes
|--------------------------------------------------------------------------
*/
Route::post('/get/chat/user', 'API\ChatController@GetAllChatUser');
Route::post('/home/user/send', 'API\ChatController@SendMessage');
Route::post('/home/user/receive', 'API\ChatController@GetMessage');

/*
|--------------------------------------------------------------------------
| Chat API Routes
|--------------------------------------------------------------------------
*/
Route::get('/get/category/data','API\ApplicationController@GetCategories');
Route::post('get/service/list','API\ApplicationController@GetAllServices');



Route::match(array('GET','POST'),'service/create', 'API\ServiceAuth\ServiceController@service_create');
// Route::post('service/create', 'API\ServiceAuth\ServiceController@service_create');	


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('/{a?}/{b?}/{c?}/{d?}/{e?}/{f?}/{g?}/{h?}/{i?}/{j?}/{k?}', function (){
    return [
        'status' => 0,
        'message' => 'The API route you were trying to access does not exist. Please recheck route and try again.',
        'error' => '404 Not Found'
    ];
});
