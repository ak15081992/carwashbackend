<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class APIController extends Controller
{
    use Traits\ResponseTrait;
    use Traits\ValidatorTrait;
}
