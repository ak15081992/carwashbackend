<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\APIController;
use App\Models\Category;
use App\Models\BodyType;

class CategoryController extends Controller
{
    
	public function getCategorylist(){
		$model= new Category();
    	$response=$model->getCategory();
    	return $response;

    }


    public function getBodytypelist(){
    	$model= new BodyType();
    	$response=$model->getBodytype();
    	return $response;

    }
}
