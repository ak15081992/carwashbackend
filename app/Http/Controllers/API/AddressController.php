<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\APIController;
use App\Models\Address;

class AddressController extends APIController
{
    
    public function add(Request $r){
    	//dd($r);
    	$model= new Address();
    	$response=$model->addAddress($r);
    	return $response;
    
    }


    public function delete(Request $r){
		$model = new Address();
		$response=$model->deleteAddress($r);
    	return $response;
	}


	public function update(Request $r){
		$model = new Address();
		$response=$model->updateAddress($r);
    	return $response;

	}

	public function getAaddresses(Request $r){
		$model = new Address();
		$response = $model->readAllAddress($r);
		return $response;

	}
} 
