<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\APIController;
use App\Models\ChatUser;
use App\Models\MessageBox;
use App\Models\User;
use App\Utils\ValidationsUtil;
use http\Message\Body;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ChatController extends APIController
{

    public function findMessageIdByChatId(string $from,String $to)
    {
        return ChatUser::where(function ($query) use($from,$to){
            $query->where('from_id', '=', $from)
                ->where('to_id', '=', $to);
        })->orWhere(function ($query) use($from,$to){

            $query->where('from_id', '=', $to)
                  ->where('to_id', '=', $from);
        })->first();
    }


    public function GetMessage(Request $r){

        $validationUtils=new ValidationsUtil();
        $validationUtils->setFields(['from_id','to_id']);
        if (!$validationUtils->hasAllFields($r->all()))
        {
            return [
                'status' =>0,
                'message'=>$validationUtils->getValidationErrorString(),
            ];
        }
        $validator = Validator::make($r->all(), [
            'from_id' =>  ['required'],
            'to_id' => ['required']
        ]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }

        $ExitChatId=$this->findMessageIdByChatId($r->from_id,$r->to_id);
        if (!$ExitChatId)
         {
             return [
                 'status' => 0,
                 'message' => 'We could not find the chatId associated with that User.',
             ];


         }else{

           $listOfMessage=MessageBox::where('message_id','=',$ExitChatId->id)->get();
           if (count($listOfMessage)>0) {
               return [
                   'status' => 1,
                   'message' => 'List of all conversation',
                   'message'=>$listOfMessage
               ];
           }else{
               return [
                   'status' => 0,
                   'message' => 'We could not find the message associated with that message id.',
               ];
           }

        }

    }


    public function SendMessage(Request $r)
    {
        $validationUtils=new ValidationsUtil();
        $validationUtils->setFields(['from_id','to_id','message']);
        if (!$validationUtils->hasAllFields($r->all()))
        {
            return [
                'status' =>0,
                'message'=>$validationUtils->getValidationErrorString(),
            ];
        }
        $validator = Validator::make($r->all(), [
            'from_id' =>  ['required'],
            'to_id' => ['required'],
            'message'=>['required']
        ]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }

        $ExitChatId=$this->findMessageIdByChatId($r->from_id,$r->to_id);
        if ($ExitChatId)
        {
            $mesagebox = new MessageBox();
            $mesagebox->body= $r->message;
            $mesagebox->user_id=$r->from_id;
            $mesagebox->message_id=$ExitChatId->id;
            $mesagebox->save();
            $data = [
                'id' => $mesagebox->id,
                'messageid' => $mesagebox->message_id,
                'body' => $mesagebox->body,
                'userId'=>$mesagebox->user_id
            ];
            return [
                'status' => 1,
                'message' => "successfully send your message to .......",
                'data' => $data
            ];


        }else{
            $chatdata=new ChatUser();
            $results=$chatdata->Create($r);
            if ($results)
            {
                $mesagebox = new MessageBox();
                $mesagebox->body= $r->message;
                $mesagebox->user_id=$r->from_id;
                $mesagebox->message_id=$results->id;
                $mesagebox->save();
                $data = [
                    'id' => $mesagebox->id,
                    'messageid' => $mesagebox->message_id,
                    'body' => $mesagebox->body,
                    'userId'=>$mesagebox->user_id
                ];
                return [
                    'status' => 1,
                    'message' => "successfully send your message to .......",
                    'data' => $data
                ];

            }else{
                return [
                    'status' => 0,
                    'message' => 'We could not message id.',
                ];
            }

        }



    }

    public function GetAllChatUser(Request $r)
    {
        $validationUtils=new ValidationsUtil();
        $validationUtils->setFields(['user_id']);
        if (!$validationUtils->hasAllFields($r->all()))
        {
            return [
                'status' =>0,
                'message'=>$validationUtils->getValidationErrorString(),
            ];
        }
        $validator = Validator::make($r->all(), [
            'user_id' =>  ['required']
        ]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }
        $results=[];
        $list_from_User=ChatUser::where('from_id','=',$r->user_id)->get();
        $list_to_User=ChatUser::where('to_id','=',$r->user_id)->get();

        $ids = [];
        foreach ($list_from_User as $record)
        {
            $ids[] = [
                'id'=>$record->from_id
            ];
        }
        foreach ($list_to_User as $record)
        {
            $ids[] = [

                'id'=>$record->from_id
            ];
        }
        if (count($ids)>0)
        {
            foreach ($ids as $id) {
                $userData=User::where('id','=',$id)->first();
                $results[] = [
                    'id'=>$userData->id,
                    'name'=>$userData->name,
                    'email' => $userData->email
                ];
            }
            if (count($results)>0){
                return [
                    'status' => 1,
                    'message' => "List of User .......",
                    'data' => $results
                ];

            }else {
                return [
                    'status' => 0,
                    'message' => 'No  user Exits',
                ];
            }

        }else{

            return [
                'status' => 0,
                'message' => 'No chat user Exits',
            ];
        }




    }



}
