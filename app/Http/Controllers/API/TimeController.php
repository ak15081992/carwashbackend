<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\APIController;
use App\Models\Time;


class TimeController extends Controller
{
	public function getTimelist(){
		$model= new Time();
    	$response=$model->TimeList();
    	return $response;
	}   
}
