<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\APIController;
use App\Models\BodyType;
use App\Models\Category;
use App\Models\Package;
use App\Models\PriceModels;
use App\Models\ServicesModels;
use App\Models\SubCategory;
use App\Utils\ValidationsUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ApplicationController extends APIController
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;

    }

    public function GetCategories(){
        $response  = array();
        $catData = Category::all();
        foreach ($catData as $value) {
            $Arr=array();
            $Arr['cat_id']=$value->cat_id;
            $Arr['name']= $value->name;
            $Arr['description']= $value->description;
            $subcategory=SubCategory::where('cat_id','=',$value->cat_id)->get();

            foreach ($subcategory as $subvalue) {
                $bodyData=BodyType::where("body_id",'=',$subvalue->body_id)->first();
                $subArr=array();
                $subArr['catId']=$subvalue->cat_id;
                $subArr['bodyId']=$subvalue->body_id;
                $subArr['subcatId']=$subvalue->id;
                $subArr['name']=$bodyData->name;
                $subArr['description']=$bodyData->description;
                $subArr['image']=$bodyData->image;
                $Arr['SubCategory'][]=$subArr;
            }
            $response[]=$Arr;
        }

        if (count($response)>0){

            return [
                'status'=>1,
                'message'=>'List of categories data',
                'data'=>$response
            ];

        }else{

            return [
                'status'=>1,
                'message'=>'categories not found data',
                'data'=>$response
            ];

        }


    }


    public function GetAllServices(Request $r)
    {
        $validationUtils=new ValidationsUtil();
        $validationUtils->setFields(['subcatId']);
        if (!$validationUtils->hasAllFields($r->all()))
        {
            return [
                'status' =>0,
                'message'=>$validationUtils->getValidationErrorString(),
            ];
        }
        $validator=Validator::make($r->all(),['subcatId' => ['required']]);
        if ($validator->fails())
        {
            return[
                'status'=>0,
                'message'=>'Your provided subcatid is not in valid ',

            ];
        }

        $response  = array();
        $priceList=PriceModels::where('sub_cat_id','=',$r->subcatId)->get();
        return [
            'status'=>1,
            'message'=>'List of categories data',
            'data'=>$priceList
        ];


//        foreach ($priceList as $price){
//            $packageData = Package::where("pack_id", '=', $price->pack_id)->first();
//            $Arr=array();
//            $Arr['pId']=$packageData->pack_id;
//            $Arr['name']= $packageData->name;
//            $Arr['description']= $packageData->description;
//            foreach() {
//                $bodyData = ServicesModels::where("ser_id", '=', $subvalue->body_id)->first();
//
//
//            }
//
//
//
//        }


    }


}
