<?php

namespace App\Utils;

use Illuminate\Http\Request;

/**
 * Utility class to validate incoming input and user input.
 * @package App\Utils
 */
class ValidationsUtil{
	/**
	 * Contains the field names that will be validated.
	 * @var array
	 */
	private $fields = [];

	/**
	 * @var null |string
	 */
	private $failedKey = null;

	/**
	 * Returns the name of key that failed last validation attempt.
	 * @return string|null
	 */
	public function getFailedKey(){
		return $this->failedKey;
	}

	/**
	 * Returns a string telling which key failed the validation ruleset.
	 * @return string
	 */
	public function getValidationErrorString(){
		return sprintf("Request does not contain %s field.", $this->getFailedKey());
	}

	/**
	 * Sets fields that will be used to validate the incoming input.
	 * @param array $fieldsArray
	 * @return $this
	 */
	public function setFields(array $fieldsArray){
		$this->fields = $fieldsArray;
		return $this;
	}

	/**
	 * ValidationsUtil constructor.
	 */
	public function __construct(){

	}

	/**
	 * Checks whether incoming request or array contains all fields (all are set or not).
	 * @param array $inputFields
	 * @return bool
	 */
	public function hasAllFields(array $inputFields){
		foreach ($this->fields as $field) {
			if (!array_key_exists($field, $inputFields)) {
				$this->failedKey = $field;
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if a field contains a non-empty, minimum 1 character value.
	 * @param $field
	 * @return bool
	 */
	public function hasNonEmptyString($field){
		return strlen(trim($field)) > 0;
	}
}