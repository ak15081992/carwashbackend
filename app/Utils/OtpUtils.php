<?php

namespace App\Utils;

use GuzzleHttp\Exception\ConnectException;
use \GuzzleHttp\Client;

/**
 * Provides functionality related to OTP generation and delivery.
 * @package App\Utils
 */
class OtpUtils{
    /**
     * Set this to the length of OTP you want to generate.
     */
    const OtpLength = 4;

    /**
     * Phone number used for authentication in SMS API.
     */

    /**
     * Password used for authentication in SMS API.
     */
    const Password = "b640d";

    /**
     * Sender ID to be displayed in messaging app.
     */

    /**
     * Base URL of SMS API.
     */
    const BaseURL = "https://2factor.in/API/R1/?module=PROMO_SMS&apikey=83bb62be-18b9-11e9-9ee8-0200cd936042&from=SENDER_ID";

    /**
     * @var string
     */
    const HashCode = "EEMHMX4ASAy";


    /**
     * Generates a random 4 to 8 digit OTP specified by OTPLength member.
     */
    public static function generate(){
        switch (self::OtpLength) {
            case 4:
                return mt_rand(1000, 9999);

            case 5:
                return mt_rand(10000, 99999);

            case 6:
                return mt_rand(100000, 999999);

            case 7:
                return mt_rand(1000000, 9999999);

            default:
                return mt_rand(10000000, 99999999);
        }
    }

    /**
     * @param $otp
     * @param $mobile
     * @return mixed
     */
    public static function sendOTP($otp, $mobile){
        return true;
//        $base = self::BaseURL . "phone=" . self::PhoneNumber . "&password=" . self::Password . "&senderid=" . self::SenderID;
//        $message = "Your one time password for login is " . $otp . ".";
//        $final = $base . "&message=" . $message . "&numbers=" . $mobile;
//        $client = new Client();
//        try {
//            $request = $client->get($final);
//            $response = $request->getBody()->getContents();
//            return true;
//        }
//        catch (ConnectException $e) {
//            return false;
//        }
    }

    /**
     * @param $otp
     * @param $mobile
     * @return array|string
     */
    public static function sendAppOtp($otp, $mobile){
        //return true;
        $message = \sprintf("<%%23> Your Carwash code is: %d %%0A %s", $otp, self::HashCode);
        $final = self::BaseURL . "&msg=" . $message . "&to=" . $mobile;
        $client = new Client();
        try {
            $request = $client->get($final);
            $response = $request->getBody()->getContents();
            return true;
        }
        catch (ConnectException $e) {
            return false;
        }
    }

    /**
     * Sends an OTP to update a mobile number.
     * @param $otp
     * @param $mobile
     * @return array|string
     */
//    public static function sendMobileUpdateOtp($otp, $mobile){
//        $base = self::BaseURL . "phone=" . self::PhoneNumber . "&password=" . self::Password . "&senderid=" . self::SenderID;
//        $message = \sprintf("<%%23> Your OTP to update mobile number is: %d %%0A %s", $otp, self::HashCode);
//        $final = $base . "&message=" . $message . "&numbers=" . $mobile;
//        $client = new Client();
//        try {
//            $request = $client->get($final);
//            $response = $request->getBody()->getContents();
//            return true;
//        }
//        catch (ConnectException $e) {
//            return false;
//        }
//    }

    /**
     * Returns a validation rule to validate OTP based on current
     * OTP length.
     * @return array
     */
    public static function getOTPValidationRule(){
        return [
            'bail',
            'required',
            sprintf("digits:%d", self::OtpLength)
        ];
    }
}
