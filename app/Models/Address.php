<?php

namespace App\Models;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use App\Utils\ValidationsUtil;

class Address extends Model
{
    protected $fillable = ['id', 'customer_id', 'house_or_Flatnumber', 'latitude', 'longitude', 'landmark', 'main_address'];

    
  



	public function addAddress(Request $r){
		$validationUtils = new ValidationsUtil();
		$validationUtils->setFields(['customer_id', 'house_or_Flatnumber', 'latitude', 'longitude', 'landmark', 'main_address']);
		if (!$validationUtils->hasAllFields($r->all())) {
			return [
				'status' => 0,
				'message' => sprintf("Request does not contain %s field.", $validationUtils->getFailedKey()),
				'addresses' => null
			];
		}

		$validator = Validator::make($r->all(), [
			'customer_id' => ['bail', 'required', 'numeric', 'exists:users,id'],
			'house_or_Flatnumber' => ['bail', 'required', 'string'],
			'main_address' => ['bail', 'required', 'string'],
			'latitude' => ['bail', 'required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
			'longitude' => ['required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
			'landmark' => ['bail', 'string', 'nullable']
		]);

		if ($validator->fails()) {
			return [
				'status' => 0,
				'message' => 'We could not validate the details sent by you.',
				'addresses' => null,
				'fields' => $validator->errors()
			];
		}
		else {
			$address = new Address();
			$address->customer_id = $r->customer_id;
			$address->house_or_Flatnumber = $r->house_or_Flatnumber;
			$address->landmark = $r->landmark;
			$address->latitude = $r->latitude;
			$address->longitude = $r->longitude;
			$address->main_address = $r->main_address;
			try {
				$address->save();
				return [
					'status' => 1,
					'message' => 'Address was saved successfully.',
					'addresses' => $address
					
				];
			}
			catch (\Exception $e) {
				return [
					'status' => 0,
					'message' => 'An error occurred when saving address.',
					'addresses' => null
				];
			}
		}
	}


	public function readAllAddress(Request $r, bool $internalCall = false){
		$validationUtils = new ValidationsUtil();
		$validationUtils->setFields(['customer_id']);
		if (!$validationUtils->hasAllFields($r->all())) {
			return [
				'status' => 1,
				'message' => sprintf("Request does not contain %s field.", $validationUtils->getFailedKey()),
			];
		}

		$customer_id = $r->customer_id;
		try {
			$addresses = Address::where('customer_id', $customer_id)->get();
			$count = count($addresses);
			if ($count >= 1) {
				$shortenedAdds = [];
				foreach ($addresses as $address) {
					$shortAddress = [
						'id' => $address->id,
						'house_or_Flatnumber' => $address->house_or_Flatnumber,
						'landmark' => $address->landmark,
						'latitude' => $address->latitude,
						'longitude' => $address->longitude,
						'main_address' => $address->main_address
					];
					$shortenedAdds[] = $shortAddress;
				}
				$response = [
					'status' => 1,
					'message' => sprintf("We found %d address(es) for this user.", $count),
					'addresses' => $shortenedAdds
				];
			}
			else {
				$response = [
					'status' => 1,
					'message' => "We did not find any address(es) for this user.",
					'addresses' => null
				];
			}
		}
		catch (\Exception $e) {
			$response = [
				'status' => 0,
				'message' => "We encountered an error trying to find any address(es) for this user.",
				'addresses' => null
			];
		}

		if ($internalCall) {
			return $response['addresses'];
		}
		else {
			return $response;
		}
	}



	public function deleteAddress(Request $r){
		$validationUtils = new ValidationsUtil();
		$validationUtils->setFields(['addressId']);
		if (!$validationUtils->hasAllFields($r->all())) {
			return [
				'status' => 1,
				'message' => sprintf("Request does not contain %s field.", $validationUtils->getFailedKey()),
			];
		}

		$addressId = $r->addressId;
		try {
			$address = Address::find($addressId);
			if ($address != null) {
				$address->delete();
				return [
					'status' => 1,
					'message' => "We successfully deleted specified address for this user."
				];
			}
			else {
				return [
					'status' => 1,
					'message' => "We could not find the address specified by address Id."
				];
			}
		}
		catch (\Exception $e) {
			return [
				'status' => 0,
				'message' => "We encountered an error trying to delete any address(es) for this user.",
			];
		}



	}


	public function updateAddress(Request $r){

		$validationUtils = new ValidationsUtil();
		$validationUtils->setFields(['customer_id', 'house_or_Flatnumber', 'latitude', 'longitude', 'landmark', 'main_address']);
		if (!$validationUtils->hasAllFields($r->all())) {
			return [
				'status' => 1,
				'message' => sprintf("Request does not contain %s field.", $validationUtils->getFailedKey()),
			];
		}

		$validator = Validator::make($r->all(), [
			'addressId' => ['bail', 'required', 'numeric'],
			'house_or_Flatnumber' => ['bail', 'string', 'nullable'],
			'latitude' => ['bail', 'required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
			'longitude' => ['required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
			'landmark' => ['bail', 'string', 'nullable'],
			'main_address' => ['bail', 'string', 'nullable'],
			

			
		]);

		if ($validator->fails()) {
			return [
				'status' => 0,
				'message' => 'We could not validate the details sent by you.',
				'fields' => $validator->errors()
			];
		}
		else {
			$address = Address::find($r->addressId);
			if ($address == null) {
				return [
					'status' => 1,
					'message' => 'We could not find the address for that address Id.',
					'addressId' => null
				];
			}
			$address->landmark = $r->landmark;
			$address->house_or_Flatnumber = $r->house_or_Flatnumber;
			$address->latitude = $r->latitude;
			$address->longitude = $r->longitude;
			$address->main_address= $r->main_address;
			try {
				$address->save();
				return [
					'status' => 1,
					'message' => 'Address was updated successfully.',
					'addressId' => $address->id
				];
			}
			catch (\Exception $e) {
				return [
					'status' => 0,
					'message' => 'An error occurred when saving address.',
					'addressId' => null
				];
			}
		}

	}


}
