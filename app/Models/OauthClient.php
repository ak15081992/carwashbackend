<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OauthClient extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'name',
        'secret',
        'redirect',
        'password_client',
        'personal_access_client',
        'revoked'
    ];

    protected $table = "oauth_clients";
}
