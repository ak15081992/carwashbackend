<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
	protected $fillable = ['id', 'time_duration'];
   
   public function TimeList(){

   	 $data = Time::select('id','time_duration')->get();
		try {
				return [
					'status' => 1,
					'message' => 'Time periods are:',
					'data' => $data
					
				];
			}
			catch (\Exception $e) {
				return [
					'status' => 0,
					'message' => 'An error occurred .'
					
				];
			}

   }
}
