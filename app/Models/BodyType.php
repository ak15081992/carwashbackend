<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BodyType extends Model
{
    protected $fillable = ['body_id','name','description','image'];


    public function getBodytype(){
    	$list = BodyType::all();
		try {
				return [
					'status' => 1,
					'message' => 'car bodytype list is',
					'btype_list' => $list
					
				];
			}
			catch (\Exception $e) {
				return [
					'status' => 0,
					'message' => 'An error occurred .',
					'btype_list' => null
				];
			}
    }
}
