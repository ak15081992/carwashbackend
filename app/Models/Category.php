<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use App\Utils\ValidationsUtil;

class Category extends Model
{
    protected $fillable = ['id', 'name', 'description','image'];


	public function getCategory(){

		$category = Category::all();
		try {
				return [
					'status' => 1,
					'message' => 'Category list is',
					'cat_list' => $category
					
				];
			}
			catch (\Exception $e) {
				return [
					'status' => 0,
					'message' => 'An error occurred.',
					'cat_list' => null
				];
			}
	}	


}
