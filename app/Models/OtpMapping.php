<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OtpMapping extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id', 'mobile', 'otp'];

    /**
     * @var string
     */
    protected $table = 'otp_mappings';

    /**
     * Matches given OTP with the one actually generated for a particular mobile number.
     * @param User $user
     * @param int $otp
     * @return bool
     */
    public static function matches(User $user, int $otp){
        //return true;
        return isset($user) ? intval($user->otp) == $otp : false;
    }
}
