<?php

use App\Models\SubCategory;
use Illuminate\Database\Seeder;

class Categoryseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <4; $i++) {

            for ($j=1;$j<10;$j++)
            {
                SubCategory::create([
                    'cat_id' => $i,
                    'body_id' => $j
                ]);

            }

        }
    }
}
