<?php

use App\Models\PriceModels;
use Illuminate\Database\Seeder;

class PriceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <28; $i++) {

            for ($j=2 ;$j<7;$j++){
                PriceModels::create([
                    'sub_cat_id' => $i,
                    'pack_id' => 2,
                    'service_id'=>$j,
                    'price'=>1000+$i
                ]);
            }



        }
    }
}
