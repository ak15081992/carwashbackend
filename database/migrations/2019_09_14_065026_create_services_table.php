<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('service_type')->nullable();
            $table->integer('service_count')->nullable();
            $table->integer('price_for_small_car')->nullable();
            $table->integer('price_for_big_car')->nullable();
            $table->integer('service_days_in_a_month')->nullable();
            $table->integer('service_days_in_a_weak')->nullable();
            $table->string('service_image')->nullable();
            $table->integer('service_hour')->nullable();
            $table->string('service_time_at')->nullable();
            $table->string('service_time2_at')->nullable();
            $table->string('service_time3_at')->nullable();
            $table->string('service_time4_at')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
